+++
author = "Christopher Gollmar"
date = "2019-08-14 10:04:00"
description = "You are regularly observing your students. Having a simple notetaking sheet is a must. Here are three templates that have worked well for me."
showFullContent = false
tags = ["classroom management", "assessment", "printable"]
title = "Three simple templates for recording observations"

+++

You are regularly observing your students throughout the day to learn more about them, assess their learning, and inform your teaching. While I have tried a number of different low-tech and computer-based notetaking systems, I find that I am most successful at regularly recording my observations when I have simple notetaking sheets at the ready.

There is no one-size-fits-all format for taking notes. I like to have copies of a couple different formats available. You can find three example sheets below.

Here are some more tips, drawn from what has worked well for me:

* Give yourself room to write the date an a brief description of the activity or learning context. This will help you file your sheet later.
* Keep the top margin extra wide so that you can use a clipboard without covering up any of your notetaking space.
* Optionally, widen the left margin if you plan to punch holes to add the sheet to a binder.
* Chances are, you've got a big class; this doesn't leave much space for each student on the sheet. Find a writing utensil that encourages you to write small. In my case, that's a mechanical pencil!
* Try starting with just two formats: a page with a single box or line for each student and a grid with rows for each student and five columns to track progress over time.

While I'll still occasionally take out a blank piece of paper for notetaking, there are advantages to working with a structured format such as these. First, it encourages you to record observations for *each student.* Even if I don't spend time with each and every student during a given lesson or work time, I can easily see whose lines/boxes are still empty and circle back to them soon. Second, it lets me quickly find notes on a particular student when looking back. Come report-writing time, when I have accumulated stacks of sheets organized by subject or skill, I can quickly note a child's progress by flipping through and looking to the *same spot on each page.* If I know I may be working from home, I will even take a whole stack of sheets to the copier and scan/email them to myself so I can flip through a PDF rather than the physical pages.

Below you'll find some mock-ups of what has worked well for me. These formats work well for up to 22 students per class. You may find that you need to tweak them in the likely case that you have more students.

## Template 1: The Big Grid

![Preview image of a large grid notetaking sheet](/uploads/notetaking-template-1.png "The Big Grid")

[Example file in Google Docs][template-1]

## Template 2: Line-by-line

![Preview image of a notetaking sheet with one row per student](/uploads/notetaking-template-2.png "Line-by-line")

[Example file in Google Docs][template-2]

## Template 3: Rows and Columns

![Preview image of a notetaking sheet with one row per student, subdivided into 5 columns](/uploads/notetaking-template-3.png "Rows and Columns")

[Example file in Google Docs][template-3]

[template-1]: https://docs.google.com/document/d/1x3iS1aKJhmNsIN8Tu0Szo4t-XFMo-QC3GVRSYakp6c4/edit?usp=sharing "Open in Google Docs"
[template-2]: https://docs.google.com/document/d/1mt5AUXXK7WMldQJXbN96QG5DqTTzjMFi3UI30zLq480/edit?usp=sharing "Open in Google Docs"
[template-3]: https://docs.google.com/document/d/1EvyUzuqwi6p-SYFYBR7tJB_jdZZpespeyTR_WgeCCbA/edit?usp=sharing "Open in Google Docs"
