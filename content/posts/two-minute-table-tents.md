---
title: Two-minute table tents
date: 2019-08-08
author: Christopher Gollmar
cover: ''
tags:
- notech
- classroom management
description: 'This is my go-to #notech solution for those times when I need to quickly
  set up a sign on a table or shelf in my classroom.'
showFullContent: false

---
When I want to place a sign on a table or shelf in my classroom -- say it's to remind students of the rules of a game or invite parents and caregivers to join an activity -- I make a quick table tent.

## Materials:

For each table tent, you will need:

* One wooden block
* Two pieces of masking tape
* One piece of paper or cardstock, letter or similar size

## Instructions:

1. Fold the piece of paper in half ([hamburger-style][hamburger-fold]).
2. Write your message on one or both sides of the paper, with the fold on top.
3. Now, your block will form a base. Tape each side of your folded paper to opposite faces of the block. 

...and you're done!

My classroom is stocked with a set of [unit blocks][blocks], so I tend to just grab one block (a unit) from the block area to set this up. In order to keep cleanup simple, I always use masking tape. I don't want to get stuck scraping Scotch tape off of a block with my fingernails!

[hamburger-fold]: https://math.okstate.edu/geoset/Dictionary/H&HFolds.htm "Definitions of hamburger and hot dog folds"

[blocks]: http://www.froebelweb.org/web2027.html "Basic info on Unit Blocks"
