---
title: About this site
subtitle: Why low-tech?
comments: false
---

Hi, I'm [Chris](https://gollmar.org/). I'm a primary-grade teacher in Brooklyn, NY. I am also a self-taught programmer, Linux user, and low-key geek.

As a teacher of young children, I'm committed to creating an intentionally low-tech classroom. I want my students to develop a curiosity about the world and other people, not a fascination with screens or addiction to apps. On the other hand, I like to use my computer skills to help me in my professional practice.

I created this website as part laboratory, part repository. I am eager to experiment with using my coding skills to create and share tools and resources with other teachers. For certain kinds of resources, the payoff is huge: I don't want to spend an hour battling Google Docs every time I want to create a new set of bingo cards or number search; I'd rather spend five hours doing it once (and share the code with you to boot).

## What You'll Find on this Website

You'll find math games, word study activities, blank templates, and other printable resources here. Some resources are all ready to print and use; others are waiting for you to create. 

This site being a laboratory of sorts, things will change. If you find something you especially like, I recommend downloading it and saving it locally. I don't intend to create user accounts where you can save favorite printables. 

This site is also intended for a more technical audience. No, you won't have to spin up a VM and run the source code yourself or chroot your school-issued Chromebook to generate math activities from the command line*, but you can expect to have to read the documentation or troubleshoot something if it's not working like it should.

*But wouldn't that be cool?

## My Design Philosophy: Keep it Simple, Sweetie

As a technologist, I'm interested in low-level tools. As a teacher, I'm interested in simplicity.

Many resources available to teachers these days -- even those created by other teachers -- are cluttered, full of clipart or extraneous text, and may be hard for beginning readers to understand. I aspire to create resources that are clean, clear, and direct. Here's what that looks like in practice:

* I use a minimal palette of fonts, with preference for letterforms that are similar to handwritten letters.
* Graphics are generally black and white for easy photocopying.
* There is no copyright or licensing information on the page.
* Illustrations are included when they serve a purpose and are not merely decorative.

Guess what? This makes it easier for you to **adapt** or **reuse** anything you see here. Don't spend any money or time searching for the perfect Valentines Day-themed math game for your kindergartners. Print one from here, slap some hearts on it, and call it a day.

## The Part About Money

I've invested time and money into this website. Not very much of either -- that's an advantage to the KISS design philosophy -- but enough that I'm asking for donations.

* My goal for the **first six months** of this site (January through June 2019) is to break even financially. 
* In the **first year**, I hope to make a modest profit -- enough where I could conceivably buy myself a fancy umbrella drink at the Tiki bar down the road.
* If everything is looking good a year in, I hope to start paying myself back for the time I invest into this site starting in January 2020.
* If I somehow strike it rich here, I'm still going to keep teaching, so I'll use the extra proceeds to buy a round of fancy umbrella drinks for collaborators, contributors, and superusers.

Will you chip in? I could use your help.

* Estimated operating expenses, 2019: $90
* Money raised so far: $0

## Want to Help Out? Report a Bug? Send a Compliment?

[Get in touch](mailto:chris@gollmar.org)!

## Credits and Acknolwedgments

Even though we may be one of just a few grown-ups in a room full of children most days, teaching is richly collaborative; we are always building off of what we've learned from our mentors, colleagues, and students. Such is also the case with this website. 

### Activities and Inspiration

* Jonathan Edmonds
* Carole Fullerton

### Fonts, Graphics, and Software

* Andika New Basic
* Flask
* WeasyPrint
* Vim 
* Ubuntu

## What About the License?

You got it. [Read to your heart's content](LICENSE.md), then share everything widely.
