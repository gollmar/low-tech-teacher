---
title: Credits
subtitle: I didn't do this alone
comments: false
---

Even though we may be one of just a few grown-ups in a room full of children most days, teaching is richly collaborative; we are always building off of what we've learned from our mentors, colleagues, and students. Such is also the case with this website. 

## Activities and Inspiration

* Jonathan Edmonds
* Carole Fullerton

## Fonts, Graphics, and Software

* Andika New Basic
* Flask
* WeasyPrint
* Vim 
* Ubuntu

## What About the License?

You got it. [Read to your heart's content](LICENSE.md), then share everything widely.
